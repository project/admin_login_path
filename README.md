CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Admin Login Path is a simple module that routes the login pages to use the
admin (theme) path.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/admin_login_path

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/admin_login_path


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed. When enabled, the module will automatically alter
the routes for user/login, user/password, user/register, account cancellation,
and password reset to use the site's administration theme.
