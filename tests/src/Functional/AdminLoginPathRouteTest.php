<?php

namespace Drupal\Tests\admin_login_path\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Tests the functionality of the Admin Login Path route subscriber.
 *
 * @group admin_login_path
 */
class AdminLoginPathRouteTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['admin_login_path'];

  /**
   * Asserts that login routes are correctly marked as admin routes.
   */
  public function testAdminRoute() {
    $login_routes = [
      'user.login',
      'user.register',
      'user.pass',
      'user.cancel_confirm',
      'user.reset.login',
      'user.reset',
      'user.reset.form',
    ];

    foreach ($login_routes as $route_name) {
      $route = \Drupal::service('router.route_provider')->getRouteByName($route_name);
      $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);
      $this->assertTrue($is_admin, new FormattableMarkup('Admin route correctly marked for "@title" page.', ['@title' => $route->getDefault('_title')]));
    }
  }

}
